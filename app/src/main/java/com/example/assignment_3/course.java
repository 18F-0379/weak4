package com.example.assignment_3;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class course extends Fragment {
    RecyclerView recyclerView;
    @SuppressLint("ResourceType")
    @Nullable
    @Override
    ArrayList<datamodule> dataholder;
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
    {

        View view=inflater.inflate(R.layout.fragment_course,container,false);
        recyclerView=view.findViewById(R.id.recycler_course);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dataholder=new ArrayList<>();
        datamodule obj1=new datamodule(R.drawable.ic_baseline_person_add_alt_1_24,"English","1");
        dataholder.add(obj1);
        datamodule obj2=new datamodule(R.drawable.ic_baseline_person_add_alt_1_24,"C++","2");
        dataholder.add(obj2);
        datamodule obj3=new datamodule(R.drawable.ic_baseline_person_add_alt_1_24,".NET","1");
        dataholder.add(obj3);
        datamodule obj4=new datamodule(R.drawable.ic_baseline_person_add_alt_1_24,"MOBILE APPLICATION","1");
        dataholder.add(obj4);
        datamodule obj5=new datamodule(R.drawable.ic_baseline_person_add_alt_1_24,"datastructer","1");
        dataholder.add(obj5);
        return view;
    }




}

