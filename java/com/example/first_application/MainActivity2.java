package com.example.first_application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {
    EditText FirstName, LastName, Email, Password, phone2, address;
    Button signup;
    boolean isAllFieldsChecked = false;
     Boolean isEmailValid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
         FirstName = findViewById(R.id.FirstName);
         LastName = findViewById(R.id.Lastname);
         Email = findViewById(R.id.Email);
         Password = findViewById(R.id.password);
         phone2 = findViewById(R.id.phone);
         address = findViewById(R.id.address);
        signup = findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAllFieldsChecked = CheckAllFields();
                if (isAllFieldsChecked) {
                    Intent intent = new Intent(MainActivity2.this, MainActivity.class);
                    startActivity(intent);

                }

            }
        });
      signup.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String Firstname=FirstName.getText().toString();
        String Lastname=LastName.getText().toString();
        String email=Email.getText().toString();
        String password=Password.getText().toString();
        String phone=phone2.getText().toString();
        String address1=address.getText().toString();
        Intent intent2=new Intent(MainActivity2.this,MainActivity.class);
        Intent intent1=new Intent(MainActivity2.this,MainActivity3.class);

        intent1.putExtra("Name",Firstname);
        intent1.putExtra("Name1",Lastname);
        intent1.putExtra("Email",email);
        intent1.putExtra("password",password);
        intent1.putExtra("phone",phone);
        intent1.putExtra("address",address1);
        intent2.putExtra("Email",email);
        intent2.putExtra("password",password);


        startActivity(intent1);
    }
});

    }


    private boolean CheckAllFields(){

        if(FirstName.length()==0){
            FirstName.setError("This Field is Required");
            return false;
        }
        if(LastName.length()==0){
            LastName.setError("This Field is Required");
            return false;
        }
        if(Email.length()==0){
            Email.setError("This Field is Required");
            return false;
        }

        if(phone2.length()==0){
            phone2.setError("This Field is Required");
            return false;
        }

        if(Password.length()==0){
            Password.setError("This Field is Required");
            return false;
        }
        else if (Password.length() < 8) {
            Password.setError("Password must be minimum 8 characters");
            return false;
        }





return true;
    }

}